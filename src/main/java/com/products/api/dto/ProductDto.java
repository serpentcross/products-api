package com.products.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Class describing a model of single Product")
public class ProductDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "Name of the Product", hidden = true)
    private UUID id;

    @NotEmpty(message = "Name must not be empty!")
    @Size(min = 2, max = 80, message = "Name must be between 2 and 80 characters long")
    @ApiModelProperty(required = true, value = "Name of the Product")
    private String name;

    @DecimalMax("999999.99")
    @ApiModelProperty(required = true, value = "Price of the Product")
    private BigDecimal price;

    @ApiModelProperty(value = "Date of creation.")
    private String date;

    @ApiModelProperty(value = "List of available prices in other currencies.")
    private List<CurrencyDto> currencyDtoList;

}