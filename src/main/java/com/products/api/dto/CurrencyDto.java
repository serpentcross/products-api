package com.products.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

import java.math.BigDecimal;

@Data
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyDto {

    @JsonProperty("CharCode")
    private String charCode;

    @JsonProperty("Value")
    private BigDecimal value;

    @JsonProperty("Name")
    private String name;

}