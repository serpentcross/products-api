package com.products.api.persistence.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Currency implements Serializable {

    private static final long serialVersionUID = 3L;

    @Id
    private String code;

    private BigDecimal price;

    private LocalDate date;

    private String description;

}