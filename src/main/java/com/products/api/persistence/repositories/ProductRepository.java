package com.products.api.persistence.repositories;

import com.products.api.persistence.entities.Product;

import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;

import java.util.List;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
    List<Product> findByPriceEquals(BigDecimal price);
}