package com.products.api.persistence.repositories;

import com.products.api.persistence.entities.Currency;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<Currency, String> {}