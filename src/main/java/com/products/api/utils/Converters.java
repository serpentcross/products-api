package com.products.api.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Converters {

    public static final String FULL_DATE_FORMAT = "yyyy-MM-dd";

    public static String convertDateToSpecificFormat(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern(FULL_DATE_FORMAT));
    }

}