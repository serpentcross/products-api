package com.products.api.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.products.api.dto.CurrencyDto;

import lombok.RequiredArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;

import java.net.URL;
import java.nio.charset.StandardCharsets;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class JsonReader {

    private final ObjectMapper objectMapper;

    public Set<CurrencyDto> readJsonFromUrl(String url) throws IOException, JSONException {
        Set<CurrencyDto> currencyDtos = new HashSet<>();
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            for (Map.Entry<String, Object> entry : new JSONObject(jsonText).getJSONObject("Valute").toMap().entrySet()) {
                currencyDtos.add(objectMapper.readValue(objectMapper.writeValueAsString(entry.getValue()), CurrencyDto.class));
            }
            return currencyDtos;
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

}