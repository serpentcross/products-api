package com.products.api.controllers;

import com.products.api.services.CurrencyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/currencies")
@Api("Set of API-methods for work with Product entity.")
public class CurrencyController {

    private final CurrencyService currencyService;

    @PutMapping
    @ApiOperation(value = "Get single Product.")
    public void updateCurrencies() throws IOException {
        currencyService.runUpdate();
    }

}