package com.products.api.controllers;

import com.products.api.exceptions.NoDataFoundException;
import com.products.api.utils.Converters;

import org.springframework.context.support.DefaultMessageSourceResolvable;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDate;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class ProductControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
        return response(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex.getClass().getName());
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<Object> handleEmptyResultException(EmptyResultDataAccessException ex, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDate.now());
        body.put("message", ex.getLocalizedMessage());

        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Object> handleNodataFoundException(NoDataFoundException ex, WebRequest request) {
        return response(HttpStatus.NOT_FOUND, ex.getMessage(), ex.getClass().getName());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return response(HttpStatus.BAD_REQUEST,
        "Error! " + ex.getBindingResult().getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList()).toString(),
            ex.getClass().getName()
        );

    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return response(HttpStatus.BAD_REQUEST,
       "Error! Price field value: " + ex.getMessage().split("\"")[1] + " must be a number in BigDecimal format!",
            ex.getClass().getName()
        );
    }

    private ResponseEntity<Object> response(HttpStatus status, String msg, String exception){
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("status", status.value());
        body.put("exception", exception);
        body.put("timestamp", Converters.convertDateToSpecificFormat(LocalDate.now()));
        body.put("message", msg);
        return new ResponseEntity<>(body, status);
    }

}