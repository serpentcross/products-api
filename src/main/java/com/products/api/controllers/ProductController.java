package com.products.api.controllers;

import com.products.api.dto.ProductDto;
import com.products.api.persistence.entities.Product;
import com.products.api.services.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
@Api("Set of API-methods for work with Product entity.")
public class ProductController {

    private final ProductService productService;

    @GetMapping("/{id}")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success!", response = ProductDto[].class),
        @ApiResponse(code = 404, message = "Nothing was found!")
    })
    @ApiOperation(value = "Get single Product.", response = Product.class)
    public Product getOne(@PathVariable UUID id) {
        return productService.getOne(id);
    }

    @GetMapping
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success!", response = ProductDto[].class),
        @ApiResponse(code = 400, message = "Price has incorrect format."),
        @ApiResponse(code = 404, message = "Nothing was found!")
    })
    @ApiOperation(value = "Get all Products.", response = Product[].class)
    public List<ProductDto> getAll(@ApiParam(name="price", value="List Products with specific price.") @RequestParam(required = false) BigDecimal price) {
        return productService.getAll(price);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success"),
        @ApiResponse(code = 500, message = "Nothing to delete")
    })
    @ApiOperation(value = "Delete single Product.")
    public void deleteOne(@PathVariable UUID id) {
        productService.deleteOne(id);
    }

    @PostMapping
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Success"),
        @ApiResponse(code = 400, message = "Validation error"),
        @ApiResponse(code = 500, message = "Data integrity error"),
    })
    @ApiOperation(value = "Persist single Product.")
    public ResponseEntity<Product> saveOne(@Valid @RequestBody ProductDto productDto) {
        return new ResponseEntity<>(productService.saveOne(productDto), HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Success"),
        @ApiResponse(code = 400, message = "Validation error"),
        @ApiResponse(code = 500, message = "Data integrity error"),
    })
    @ApiOperation(value = "Update single Product")
    public ResponseEntity<Product> updateOne(@Valid @RequestBody ProductDto productDtoNew, @PathVariable UUID id) {
        return new ResponseEntity<>(productService.updateOne(productDtoNew, id), HttpStatus.OK);
    }

}