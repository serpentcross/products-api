package com.products.api.services;

import com.products.api.dto.CurrencyDto;
import com.products.api.persistence.entities.Currency;
import com.products.api.persistence.repositories.CurrencyRepository;
import com.products.api.utils.JsonReader;

import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final JsonReader jsonReader;

    public void runUpdate() throws IOException {
        jsonReader.readJsonFromUrl("https://www.cbr-xml-daily.ru/daily_json.js")
            .stream()
            .map(currencyDto ->
                Currency.builder()
                    .code(currencyDto.getCharCode())
                    .price(currencyDto.getValue())
                    .date(LocalDate.now())
                    .description(currencyDto.getName())
                .build()
            ).forEach(currencyRepository::save);
    }

}
