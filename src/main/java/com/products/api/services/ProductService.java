package com.products.api.services;

import com.products.api.dto.CurrencyDto;
import com.products.api.dto.ProductDto;
import com.products.api.exceptions.NoDataFoundException;
import com.products.api.persistence.entities.Currency;
import com.products.api.persistence.entities.Product;
import com.products.api.persistence.repositories.ProductRepository;
import com.products.api.utils.Converters;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.math.RoundingMode;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public Product getOne(UUID id) {
        return productRepository.findById(id).orElseThrow(()-> new NoDataFoundException(id));
    }

    public List<ProductDto> getAll(BigDecimal price) {

        List<ProductDto> productDtos = new ArrayList<>();
        List<CurrencyDto> currencyDtos = new ArrayList<>();

        for (Product product : price == null ? productRepository.findAll() : productRepository.findByPriceEquals(price)) {
            for (Currency currency : product.getCurrencies()) {
                currencyDtos.add(CurrencyDto.builder()
                    .charCode(currency.getCode())
                    .name(currency.getDescription())
                    .value(calculatePrice(product.getPrice(), currency.getPrice()))
                .build());
            }

            productDtos.add(
                ProductDto.builder()
                    .id(product.getId())
                    .name(product.getName())
                    .date(Converters.convertDateToSpecificFormat(product.getDate()))
                    .price(product.getPrice())
                        .currencyDtoList(currencyDtos)
                .build()
            );
            currencyDtos = new ArrayList<>();
        }


        return productDtos;
    }

    public void deleteOne(UUID id) {
        productRepository.deleteById(id);
    }

    public Product saveOne(ProductDto productDto) {
        return productRepository.save(
            Product.builder()
                .date(LocalDate.now())
                .name(productDto.getName())
                .price(productDto.getPrice())
            .build()
        );
    }

    public Product updateOne(ProductDto productDtoNew, UUID id) {
        return productRepository.findById(id).map(product -> {
            product.setName(productDtoNew.getName());
            product.setPrice(productDtoNew.getPrice());
            return productRepository.save(product);
        }).orElseGet(() -> {
            productDtoNew.setId(id);
            return saveOne(productDtoNew);
        });
    }

    private BigDecimal calculatePrice(BigDecimal rubPrice, BigDecimal currencyPrice) {
        return rubPrice.divide(currencyPrice, 2, RoundingMode.CEILING);
    }

}