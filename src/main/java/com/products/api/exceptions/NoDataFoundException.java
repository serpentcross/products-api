package com.products.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NoDataFoundException extends RuntimeException {

    public NoDataFoundException() {
        super("Error! Nothing was found!");
    }

    public NoDataFoundException(UUID id) {
        super("Error! No entity with id " + id + " found!");
    }

}