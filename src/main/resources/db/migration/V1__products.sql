CREATE SCHEMA IF NOT EXISTS public;

DROP TABLE IF EXISTS product;

CREATE TABLE IF NOT EXISTS product (
    id UUID NOT NULL UNIQUE CONSTRAINT PK_product PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL,
    price NUMERIC NOT NULL,
    date TIMESTAMP DEFAULT now() NOT NULL
);

INSERT INTO product (id, name, price, date) VALUES ('056303f0-ff23-46f0-8f19-d838658d3228', 'Bread', 500, '2020-10-20 23:08:38.564150');
INSERT INTO product (id, name, price, date) VALUES ('8e790af9-5aac-48d2-9cf1-df3e9fd6d73d', 'Milk', 600, '2020-10-21 13:58:50.135119');
INSERT INTO product (id, name, price, date) VALUES ('901afb14-f609-4063-8781-9215e709973e', 'Sugar', 100, '2020-10-21 13:58:50.135119');
INSERT INTO product (id, name, price, date) VALUES ('95e514e5-28d6-4d89-852a-b9e23fcbed23', 'Tea', 200, '2020-10-21 13:58:50.135119');

CREATE TABLE IF NOT EXISTS currency (
    code VARCHAR NOT NULL UNIQUE CONSTRAINT PK_currency PRIMARY KEY,
    price NUMERIC NOT NULL,
    date TIMESTAMP DEFAULT now() NOT NULL,
    description VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS currency_product (
    currency VARCHAR NOT NULL CONSTRAINT "FK_currency_product_currency" REFERENCES currency,
    product UUID NOT NULL CONSTRAINT "FK_currency_product_product" REFERENCES product
);

--INSERT INTO currency_product (currency, product) VALUES ("","");

--KZT,056303f0-ff23-46f0-8f19-d838658d3228
--UAH,8e790af9-5aac-48d2-9cf1-df3e9fd6d73d
--USD,901afb14-f609-4063-8781-9215e709973e
--CZK,95e514e5-28d6-4d89-852a-b9e23fcbed23
--USD,8e790af9-5aac-48d2-9cf1-df3e9fd6d73d
--KZT,95e514e5-28d6-4d89-852a-b9e23fcbed23
--UAH,901afb14-f609-4063-8781-9215e709973e
--CZK,056303f0-ff23-46f0-8f19-d838658d3228
