package com.products.api.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.products.api.dto.ProductDto;
import com.products.api.persistence.entities.Product;
import com.products.api.services.ProductService;

import com.products.api.utils.ObjectMapperUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    private String productDtoRequest;
    private String productsResponse;
    private String productsFilteredResponse;

    private final static ObjectMapper objectMapper = ObjectMapperUtils.objectMapper();

    @Before
    public void setUp() throws IOException {

        List<ProductDto> products = objectMapper.readValue(new ClassPathResource("mocks/products.json").getFile(), new TypeReference<>() {});
        productsResponse = objectMapper.writeValueAsString(products);
        given(productService.getAll(null)).willReturn(products);

        List<ProductDto> productsFiltered = objectMapper.readValue(new ClassPathResource("mocks/products_filtered.json").getFile(), new TypeReference<>() {});
        productsFilteredResponse = objectMapper.writeValueAsString(productsFiltered);
        given(productService.getAll(new BigDecimal("500.00"))).willReturn(productsFiltered);

        Product product = objectMapper.readValue(new ClassPathResource("mocks/product.json").getFile(), Product.class);
        productDtoRequest = objectMapper.writeValueAsString(product);

        given(productService.getOne(UUID.fromString("b6a94103-605a-463e-910e-a1eb518dd55e"))).willReturn(product);

    }

    @Test
    public void testMustSuccessfullyReturnAllProducts() throws Exception {
        mockMvc
            .perform(get("/products")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json(productsResponse))
            .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    public void testMustSuccessfullyReturnAllProductsFilteredByPrice() throws Exception {
        mockMvc
            .perform(get("/products")
            .param("price", "500.00")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().json(productsFilteredResponse))
            .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void testMustReturnCorrectProductByGivenId() throws Exception {
        mockMvc
            .perform(get("/products/{id}", "b6a94103-605a-463e-910e-a1eb518dd55e")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value("Pepsi"))
            .andExpect(jsonPath("$.price").value(89.00))
            .andExpect(jsonPath("$.date").value("2020-12-20"));
    }

    @Test
    public void testMustSuccessfullySaveNewProduct() throws Exception {
        mockMvc
            .perform(post("/products")
            .content(productDtoRequest)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

        verify(productService, times(1)).saveOne(any(ProductDto.class));
        verifyNoMoreInteractions(productService);

    }

    @Test
    public void testMustDeleteProductByGivenId() throws Exception {
        mockMvc
            .perform(delete("/products/{id}", "b6a94103-605a-463e-910e-a1eb518dd55e")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        verify(productService, times(1)).deleteOne(any(UUID.class));
        verifyNoMoreInteractions(productService);

    }

    @Test
    public void testMustUpdateProductByGivenId() throws Exception {
        mockMvc
            .perform(patch("/products/{id}", "b6a94103-605a-463e-910e-a1eb518dd55e")
            .content(productDtoRequest)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

        verify(productService, times(1)).updateOne(any(ProductDto.class),any(UUID.class));
        verifyNoMoreInteractions(productService);

    }

}