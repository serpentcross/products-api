package com.products.api.services;

import com.products.api.dto.ProductDto;
import com.products.api.exceptions.NoDataFoundException;
import com.products.api.persistence.entities.Product;
import com.products.api.utils.Converters;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void mustFindTwoProductsFilteredByPrice () {

        ProductDto productDto1 = ProductDto.builder()
            .name("Pepsi")
            .price(new BigDecimal("89.00"))
            .date(Converters.convertDateToSpecificFormat(LocalDate.now()))
        .build();

        ProductDto productDto2 = ProductDto.builder()
            .name("Coke")
            .price(new BigDecimal("100.00"))
            .date(Converters.convertDateToSpecificFormat(LocalDate.now()))
        .build();

        ProductDto productDto3 = ProductDto.builder()
            .name("Fanta")
            .price(new BigDecimal("100.00"))
            .date(Converters.convertDateToSpecificFormat(LocalDate.now()))
        .build();

        productService.saveOne(productDto1);
        productService.saveOne(productDto2);
        productService.saveOne(productDto3);

       // assertEquals(2, productService.getAll(new BigDecimal("100.00")).size());

    }

    @Test
    public void mustSuccessfullyUpdatePersistedProduct() {

        ProductDto productDto1 = ProductDto.builder()
            .name("Pepsi")
            .price(new BigDecimal("89.00"))
            .date(Converters.convertDateToSpecificFormat(LocalDate.now()))
        .build();

        Product product = productService.saveOne(productDto1);

        ProductDto productDto2 = ProductDto.builder()
            .name("Cola")
            .price(new BigDecimal("189.00"))
            .date(Converters.convertDateToSpecificFormat(LocalDate.now()))
        .build();

        productService.updateOne(productDto2, product.getId());

        assertEquals("Cola", productService.getOne(product.getId()).getName());

    }

    @Test(expected = NoDataFoundException.class)
    public void mustReturnNoDataFoundException() {
        productService.getOne(UUID.randomUUID());
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void mustReturnEmptyResultDataAccessException() {
        productService.deleteOne(UUID.randomUUID());
    }

}