package com.products.api.persistence.repositories;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.products.api.dto.ProductDto;
import com.products.api.persistence.entities.Product;

import com.products.api.utils.ObjectMapperUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@DataJpaTest
@RunWith(SpringRunner.class)
public class ProductRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ProductRepository productRepository;

    private ProductDto productDto;

    private final ObjectMapper objectMapper = ObjectMapperUtils.objectMapper();

    @Before
    public void setUp() throws IOException {

        productDto = objectMapper.readValue(new ClassPathResource("mocks/product.json").getFile(), ProductDto.class);

        List<Product> productMocks = objectMapper.readValue(new ClassPathResource("mocks/products.json").getFile(), new TypeReference<>() {});

        productMocks.forEach(product -> {
            product.setId(null);
            testEntityManager.persist(product);
        });

    }

    @Test
    public void mustFindAllProducts() {
        Assert.assertEquals(4, productRepository.findAll().size());
    }

    @Test
    public void mustFindOneProduct() {
        Assert.assertEquals(1, productRepository.findByPriceEquals(new BigDecimal("100.00")).size());
    }

    @Test
    public void mustPersistProduct() {

        Product product = Product.builder()
            .name(productDto.getName())
            .price(productDto.getPrice())
            //.date(productDto.getDate())
        .build();

        Assert.assertEquals(product, productRepository.save(product));
        Assert.assertEquals(5, productRepository.findAll().size());

    }

}